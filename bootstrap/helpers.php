<?php

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;

function success_response(?object $data , int $status , string $message = '') : object
{
    return response()->json(['data' => $data , 'success' => true , 'status' => $status , 'message' => $message , 'errors' => ''] , $status);
}

function failed_response(?object $data , int $status , string $message = '' , array|string $errors = '') : object
{
    return response()->json(['data' => $data , 'success' => false , 'status' => $status , 'message' => $message , 'errors' => $errors] , $status);
}

function file_url(?string $file_name): ?string
{
    return $file_name ? '/storage/files/'.$file_name : null;
//    return $file_name ? Storage::disk('public')->url("files/".$file_name) : null;
}

function public_file_url(string $file_name): ?string
{
//    return $file_name ? URL::asset('storage/files/'.$file_name) : null;
    return $file_name ? '/'.$file_name : null;
}
