<?php

namespace Tests\Feature\controller\v1;

use App\Models\SentCode;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Artisan;
use Tests\TestCase;

class OtpControllerTest extends TestCase
{

    public function __construct()
    {
        parent::__construct();
//        Artisan::call('install');
    }
    use DatabaseMigrations;
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_user_can_get_otp_code()
    {
        $request = $this->PostJson('/api/v1/otp/send-code' , ['phone' => '09211089564']);
        $request->assertStatus(200)->assertJson([
            "data"=> null,
            "success"=> true,
            "status"=> 200,
            "message"=> "otp code sent successfully"
        ]);
    }

    public function test_user_can_validate_otp_code()
    {
        $request = $this->PostJson('/api/v1/otp/validate-code' , ['phone' => '09211089564' , 'code' => 1111]);
        $request->assertStatus(200);

    }

    public function test_example()
    {
        $user = User::factory()->create();
        SentCode::create(['user_id' => $user->id , 'phone' => $user->phone , 'code' => 1111]);

        $sentCode = SentCode::where(['phone' => $user->phone , 'code' => 1111])->get();

        $this->assertEquals(1111 , $sentCode->last()->code);
        $this->assertEquals(1 , $sentCode->count());
    }

}
