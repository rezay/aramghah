<?php

namespace App\Http\Controllers\api\v1;

use App\Http\Controllers\Controller;
use App\Http\Resources\v1\CityCollection;
use App\Http\Resources\v1\CityResource;
use App\Models\City;
use Illuminate\Http\Request;
use function response;

class CityController extends Controller
{
    public function index(Request $request)
    {
        $cities = City::query();
        if($request->has('province_id'))
        {
            $cities = $cities->where('province_id' , $request->province_id);
        }
        $cities = $cities->get();
        return success_response(new CityCollection($cities) , 200);
    }

    public function show(City $city)
    {
        return success_response(new CityResource($city) , 200);
    }
}
