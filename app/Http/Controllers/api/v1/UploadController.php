<?php

namespace App\Http\Controllers\api\v1;

use App\Http\Controllers\Controller;
use App\Http\Requests\v1\UploadStoreRequest;
use App\Http\Resources\v1\UploadCollection;
use App\Http\Resources\v1\UploadResource;
use App\Models\File;
use App\Services\v1\FileService\FileServiceFacade;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UploadController extends Controller
{
    public function show(File $upload): object
    {
        if($upload->user_id == Auth::user()->id)
        {
            return success_response(new UploadResource($upload) ,200);
        }
        return failed_response(null , 403 , 'access denied');
    }

    public function showWithHash(Request $request): object
    {
        $file_hash = $request->hash;
        $file = File::where('file_hash' , $file_hash)->first();
        if($file->user_id == Auth::user()->id)
        {
            return success_response(new UploadResource($file) ,200);
        }
        return failed_response(null , 403 , 'access denied');
    }

    public function store(UploadStoreRequest $request): object
    {
        $uploads = FileServiceFacade::store($request->file('files') , Auth::user());
        return success_response(new UploadCollection($uploads) , 200 , 'files uploaded successfully');
    }

    public function delete(File $upload)
    {

    }
}
