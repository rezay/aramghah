<?php

namespace App\Http\Controllers\api\v1;

use App\Http\Controllers\Controller;
use App\Http\Requests\v1\DeceasedStoreRequest;
use App\Http\Requests\v1\DeceasedUpdateRequest;
use App\Http\Resources\v1\DeceasedCollection;
use App\Http\Resources\v1\DeceasedResource;
use App\Models\Deceased;
use App\Services\v1\DeceasedService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DeceasedController extends Controller
{
    private DeceasedService $deceasedService;

    public function __construct(DeceasedService $deceasedService)
    {
        $this->deceasedService = $deceasedService;
    }
    public function index(Request $request)
    {
        $deceaseds = Deceased::where('user_id' , Auth::user()->id)->paginate($request->per_page);
        return success_response(new DeceasedCollection($deceaseds) , 200);
    }

    public function store(DeceasedStoreRequest $request)
    {
        $deceased = $this->deceasedService->store($request->all() , Auth::user());
        return success_response(new DeceasedResource($deceased) , 200 , 'record created successfully');
    }

    public function show(Deceased $deceased)
    {
        return success_response(new DeceasedResource($deceased) , 200 , '');
    }

    public function update(DeceasedUpdateRequest $request, Deceased $deceased)
    {
        $deceased = $this->deceasedService->update($request->all() , $deceased);
        return success_response(new DeceasedResource($deceased) , 200 , 'record updated successfully');
    }

    public function destroy(Deceased $deceased)
    {
        $deceased->delete();
        return success_response(null , 200 , 'record deleted successfully');
    }

    public function search(Request $request)
    {
        $deceaseds = Deceased::paginate($request->per_page);
        return success_response(new DeceasedCollection($deceaseds) , 200);
    }
}
