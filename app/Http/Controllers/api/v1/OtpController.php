<?php

namespace App\Http\Controllers\api\v1;

use App\Http\Controllers\Controller;
use App\Http\Requests\v1\OtpSendCodeRequest;
use App\Http\Requests\v1\OtpValidateCodeRequest;
use App\Models\SentCode;
use App\Models\User;
use function response;

class OtpController extends Controller
{
    public function sendCode(OtpSendCodeRequest $request)
    {
        $phone = $request->phone;
        $user = User::where('phone' , $phone)->first();
        if(! $user)
        {
            $user = User::create(['phone' => $phone ]);
        }
//        $code = rand(1000 , 9999);
        $code = 1111;
        //send code

        $sentCode = SentCode::where('phone' , $request->phone)->first();
        if( ! $sentCode)
            SentCode::create(['user_id' => $user->id , 'phone' => $phone , 'code' => $code]);

        return success_response(null , 200 , 'otp code sent successfully');
    }

    public function validateCode(OtpValidateCodeRequest $request)
    {
        $user = User::where('phone' , $request->phone)->first();
        $sentCode = SentCode::where('phone' , $request->phone)->where('code' , $request->code)->first();
        if( ! $sentCode)
        {
            return failed_response(null , 403 , 'invalid code');
        }
        //            $sentCode->delete();
        $accessToken = $user->createToken('accessToken')->accessToken;
        return success_response((object)['user' => $user,'access_token' => $accessToken] , 200);
    }
}
