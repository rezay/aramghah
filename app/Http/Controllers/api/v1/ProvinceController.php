<?php

namespace App\Http\Controllers\api\v1;

use App\Http\Controllers\Controller;
use App\Http\Resources\v1\ProvinceCollection;
use App\Http\Resources\v1\ProvinceResource;
use App\Models\Province;
use function response;

class ProvinceController extends Controller
{
    public function index()
    {
        $provinces = Province::all();
        return success_response(new ProvinceCollection($provinces) , 200);
    }

    public function show(Province $province)
    {
        return success_response(new ProvinceResource($province) , 200);
    }
}
