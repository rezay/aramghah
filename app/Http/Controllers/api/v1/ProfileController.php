<?php

namespace App\Http\Controllers\api\v1;

use App\Exceptions\EmptyProfileException;
use App\Http\Controllers\Controller;
use App\Http\Requests\v1\ProfileStoreRequest;
use App\Http\Requests\v1\ProfileUpdateRequest;
use App\Http\Resources\v1\ProfileResource;
use App\Services\v1\ProfileService;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    private $profileService;

    public function __construct(ProfileService $profileService)
    {
        $this->profileService = $profileService;
    }

    public function store(ProfileStoreRequest $request): object
    {
        $profile = $this->profileService->store($request->all() , Auth::user());
        return success_response(new ProfileResource($profile) , 200 , 'profile created successfully');
    }

    public function show() : object
    {
        $profile = Auth::user()->profile()->first();
        if(! $profile)
        {
            throw new EmptyProfileException();
        }
        return success_response(new ProfileResource($profile) , 200 );
    }

    public function update(ProfileUpdateRequest $request) : object
    {
        $newProfile = $this->profileService->update(Auth::user() , $request->all());
        return success_response(new ProfileResource($newProfile) , 200 , 'profile updated successfully');
    }
}
