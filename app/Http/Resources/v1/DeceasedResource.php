<?php

namespace App\Http\Resources\v1;

use App\Models\City;
use App\Models\Province;
use App\Services\v1\FileService\FileServiceFacade;
use Illuminate\Http\Resources\Json\JsonResource;

class DeceasedResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'father_name' => $this->last_name,
            'birth_date' => $this->birth_date,
            'death_date' => $this->death_date,
            'deceased_photo' => $this->deceased_photo ? [
                'id' => $this->deceased_photo,
                'url' => file_url(FileServiceFacade::get($this->deceased_photo)?->file_name)
            ] : null,
            'province_id' => $this->province_id ? [
                'id' => $this->province_id,
                'name' => $this->province()->first()->name
            ] : null,
            'city_id' => $this->city_id ? [
                'id' => $this->city_id,
                'name' => $this->city()->first()->name
            ] : null,
            'grave_site' => $this->grave_site,
            'grave_place' => $this->grave_place,
            'grave_row' => $this->grave_row,
            'grave_number' => $this->grave_number,
            'death_cause_id' => $this->death_cause_id ? [
                'id' => $this->death_cause_id,
                'name' => $this->deathCause()->first()->name
            ] : null,
            'death_cause_description' => $this->death_cause_description
        ];
    }
}
