<?php

namespace App\Http\Resources\v1;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

class UploadResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'upload_id' => $this->id,
            'file_name' => $this->file_name,
            'size' => $this->size,
            'file_hash' => $this->file_hash,
            'url' => file_url($this->file_name)
        ];
    }
}
