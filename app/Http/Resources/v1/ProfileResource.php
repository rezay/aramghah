<?php

namespace App\Http\Resources\v1;

use App\Services\v1\FileService\FileServiceFacade;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;

class ProfileResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $user = Auth::user();
        return [
            'phone' => $user->phone,
            'email' => $user->email,
            'father_name' => $this->father_name,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'nationality_id' => $this->nationality_id,
            'birth_date' => $this->birth_date,
            'province_id' => $this->province_id ? [
                'id' => $this->province_id,
                'name' => $this->province()->first()->name
            ] : null,
            'city_id'=> $this->city_id ? [
                'id' => $this->city_id,
                'name' => $this->city()->first()->name
            ] : null,
            'avatar' =>  $this->avatar ? [
                'id' => $this->avatar,
                'url' => file_url(FileServiceFacade::get(1)?->file_name) ?? public_file_url(DEFAULT_AVATAR)
            ] : null,
        ];
    }
}
