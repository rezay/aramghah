<?php

namespace App\Http\Requests\v1;

use App\Models\Deceased;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class DeceasedUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(Deceased $deceased)
    {
        return true;
        if($deceased->user_id == Auth::user()->id)
        {
            return true;
        }
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'father_name' => 'required|string',
            'birth_date' => 'required|date',
            'death_date' => 'required|date',
            'deceased_photo' => 'present|nullable|exists:files,id',
            'city_id' => 'required|exists:cities,id',
            'grave_site' => 'required',
            'grave_place' => 'present|nullable',
            'grave_row' => 'present|nullable',
            'grave_number' => 'present|nullable|integer',
            'death_cause_id' => 'required|exists:death_causes,id',
            'death_cause_description' => 'required_if:death_cause_id,999'
        ];
    }
}
