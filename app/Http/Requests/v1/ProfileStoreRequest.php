<?php

namespace App\Http\Requests\v1;

use App\Models\File;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfileStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(Request $request)
    {
        if($request->avatar)
        {
            $file = File::find($request->avatar);
            if($file && Auth::user()->id == $file->user_id)
            {
                return true;
            }
            return false;
        }
        return true;
    }

    protected function failedAuthorization()
    {
        throw new \Illuminate\Auth\Access\AuthorizationException('avatar ID is not for current user');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'father_name' => 'present|nullable|string',
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'nationality_id' => 'present|nullable|unique:profiles,nationality_id|digits:10',
            'birth_date' => 'present|nullable|date',
            'city_id' => 'present|nullable|exists:cities,id',
            'avatar' => 'present|nullable|exists:files,id'
        ];
    }
}
