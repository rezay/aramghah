<?php

namespace App\Http\Requests\v1;

use Illuminate\Foundation\Http\FormRequest;

class DeceasedStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'father_name' => 'required|string',
            'birth_date' => 'required|date',
            'death_date' => 'required|date',
            'deceased_photo' => 'present|nullable|exists:files,id',
            'city_id' => 'required|exists:cities,id',
            'grave_site' => 'required',
            'grave_place' => 'present|nullable',
            'grave_row' => 'present|nullable',
            'grave_number' => 'present|nullable|integer',
            'death_cause_id' => 'required|exists:death_causes,id',
            'death_cause_description' => 'required_if:death_cause_id,999'
        ];
    }
}
