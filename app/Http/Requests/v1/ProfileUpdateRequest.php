<?php

namespace App\Http\Requests\v1;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class ProfileUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'father_name' => 'present|nullable|string',
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'nationality_id' => 'present|nullable|digits:10|unique:profiles,nationality_id,'.Auth::user()->id.',user_id',
            'birth_date' => 'present|nullable|date',
            'city_id' => 'present|nullable|exists:cities,id',
            'avatar' => 'present|nullable|exists:files,id'
        ];
    }
}
