<?php

namespace App\Services\v1;

use App\Exceptions\DuplicateRecordException;
use App\Models\City;
use App\Models\Profile;

class ProfileService
{
    public function store(array $profileData , object $user) : object
    {
        $profileData = [
            'user_id' => $user->id,
            'father_name' => $profileData['father_name'],
            'first_name' => $profileData['first_name'],
            'last_name' => $profileData['last_name'],
            'nationality_id' => $profileData['nationality_id'],
            'birth_date' => $profileData['birth_date'],
            'province_id' => $profileData['city_id'] ? City::find($profileData['city_id'])->province()->first()->id : null,
            'city_id' => $profileData['city_id'],
            'avatar' => $profileData['avatar']
        ];
//        $profile = Profile::where('user_id' , $profileData['user_id'])->first();
        $profile = $user->profile()->first();
        if($profile)
        {
            throw new DuplicateRecordException();
        }
        return Profile::create($profileData);
    }

    public function update(object $user , array $profileData)
    {
        $profile = [
            'user_id' => $user->id,
            'father_name' => $profileData['father_name'],
            'first_name' => $profileData['first_name'],
            'last_name' => $profileData['last_name'],
            'nationality_id' => $profileData['nationality_id'],
            'birth_date' => $profileData['birth_date'],
            'province_id' => $profileData['city_id'] ? City::find($profileData['city_id'])->province()->first()->id : null,
            'city_id' => $profileData['city_id'],
            'avatar' => $profileData['avatar']
        ];
        $user->profile->update($profile);
        return $user->profile()->first();
    }
}
