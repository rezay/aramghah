<?php

namespace App\Services\v1\FileService;

use Illuminate\Support\Facades\Facade;

/**
 * @method static store(array|\Illuminate\Http\UploadedFile|\Illuminate\Http\UploadedFile[]|null $file, $id)
 */
class FileServiceFacade extends Facade
{

    protected static function getFacadeAccessor(): string
    {
        return FileService::class;
    }
}
