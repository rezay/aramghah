<?php

namespace App\Services\v1\FileService;

use App\Models\File;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class FileService
{

    public function get(int $fileId)
    {
        return File::find($fileId);
    }
    public function store(array $files , object $user):array
    {
        $uploads = [];
        DB::transaction(function() use ($files , $user , &$uploads){
            foreach($files as $key => $file)
            {
                $image_uploaded_path = $file->store('files', 'public');

                $data = [
                    'original_name' => $file->getClientOriginalName(),
                    'path' => Storage::disk('local')->url($image_uploaded_path),
                    'disk' => 'local',
                    'user_id' => $user->id,
                    'mime_type' => $file->getClientMimeType(),
                    'size' => $file->getSize(),
                    'file_name' => $file->hashName(),
                    'file_hash' => base64_encode($file->hashName()),
                    'collection' => 'general',
                    'description' => '',
                ];
                $uploaded = File::create($data);

                $uploads[] = $uploaded;
            }
        } , 2);

        return $uploads;
    }

    public function delete()
    {

    }
}
