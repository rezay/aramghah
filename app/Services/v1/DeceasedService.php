<?php

namespace App\Services\v1;

use App\Models\City;
use App\Models\Deceased;

class DeceasedService
{
    public function store(array $deceased , object $user)
    {
        $data = [
            'user_id' => $user->id,
            'first_name' => $deceased['first_name'],
            'last_name' => $deceased['last_name'],
            'father_name' => $deceased['father_name'],
            'birth_date' => $deceased['birth_date'],
            'death_date' => $deceased['death_date'],
            'deceased_photo' => $deceased['deceased_photo'],
            'province_id' => $deceased['city_id'] ? City::find($deceased['city_id'])->province()->first()->id : null,
            'city_id' => $deceased['city_id'],
            'grave_site' => $deceased['grave_site'],
            'grave_place' => $deceased['grave_place'],
            'grave_row' => $deceased['grave_row'],
            'grave_number' => $deceased['grave_number'],
            'death_cause_id' => $deceased['death_cause_id'],
            'death_cause_description' => $deceased['death_cause_description']
        ];
        return Deceased::create($data);

    }

    public function update(array $deceasedData , object $deceased)
    {
        $profile = [
            'first_name' => $deceasedData['first_name'],
            'last_name' => $deceasedData['last_name'],
            'father_name' => $deceasedData['father_name'],
            'birth_date' => $deceasedData['birth_date'],
            'death_date' => $deceasedData['death_date'],
            'deceased_photo' => $deceasedData['deceased_photo'],
            'province_id' => $deceasedData['city_id'] ? City::find($deceasedData['city_id'])->province()->first()->id : null,
            'city_id' => $deceasedData['city_id'],
            'grave_site' => $deceasedData['grave_site'],
            'grave_place' => $deceasedData['grave_place'],
            'grave_row' => $deceasedData['grave_row'],
            'grave_number' => $deceasedData['grave_number'],
            'death_cause_id' => $deceasedData['death_cause_id'],
            'death_cause_description' => $deceasedData['death_cause_description']
        ];
        $deceased->update($profile);
        return $deceased;
    }
}
