<?php

namespace App\Exceptions;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Support\Arr;
use Illuminate\Validation\ValidationException;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array<int, class-string<Throwable>>
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];


    protected function unsuccessfulOperationResponse($message , $status , $errors = [])
    {
        return failed_response(null , $status , $message , $errors);
    }

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }

    public function render($request, $e)
    {
        if ($request->is('api/*')) {
            if ($e instanceof ModelNotFoundException) {
                return $this->unsuccessfulOperationResponse('Not Found' , 404);
            }
            if ($e instanceof AuthenticationException) {
                return $this->unsuccessfulOperationResponse('Unauthenticated' , 401);
            }
            if ($e instanceof ValidationException) {
                return $this->unsuccessfulOperationResponse($e->getMessage() , $e->status , $e->errors());
            }
            if ($e instanceof AuthorizationException) {
                return $this->unsuccessfulOperationResponse($e->getMessage() , 403);
            }
            if ($e instanceof AccessDeniedHttpException) {
                return $this->unsuccessfulOperationResponse($e->getMessage() , 403);
            }
        }

        return parent::render($request, $e);
    }

    private function transformErrors(ValidationException $exception)
    {
        $errors = [];
        foreach ($exception->errors() as $field => $message) {
            $errors[] = [
                'field' => $field,
                'message' => $message[0],
            ];
        }
        return $errors;
    }

}
