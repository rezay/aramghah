<?php

namespace App\Exceptions;

use Exception;

class DuplicateRecordException extends Exception
{
    public function render($request)
    {
        return failed_response(null , 403 , 'this record is already exists');
    }
}
