<?php

namespace App\Exceptions;

use Exception;

class EmptyProfileException extends Exception
{
    public function render($request)
    {
        return failed_response(null , 403 , 'user profile is empty');
    }
}
