<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    use HasFactory;
    protected $fillable = [
        'user_id',
        'father_name',
        'first_name',
        'last_name',
        'nationality_id',
        'birth_date',
        'province_id',
        'city_id',
        'avatar'
    ];

    public function province() : object
    {
        return $this->belongsTo(Province::Class);
    }

    public function city() : object
    {
        return $this->belongsTo(City::Class);
    }

    public function avatar() : object
    {
        return $this->belongsTo(File::Class , 'id' , 'avatar');
    }

    public function getBirthDateAttribute($value)
    {
        return date('Y-m-d' , $value);
    }

    public function setBirthDateAttribute($value)
    {
        $this->attributes['birth_date'] = strtotime($value);
    }
}
