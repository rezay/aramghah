<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Deceased extends Model
{
    use HasFactory,SoftDeletes;

    protected $perPage = PER_PAGE_DEFAULT;

    protected $fillable=[
        'user_id',
        'first_name',
        'last_name',
        'father_name',
        'birth_date',
        'death_date',
        'deceased_photo',
        'province_id',
        'city_id',
        'grave_site',
        'grave_place',
        'grave_row',
        'grave_number',
        'death_cause_id',
        'death_cause_description'
    ];

    public function province() : object
    {
        return $this->belongsTo(Province::Class);
    }

    public function city() : object
    {
        return $this->belongsTo(City::Class);
    }

    public function deathCause():object
    {
        return $this->belongsTo(DeathCause::Class);
    }

    public function getBirthDateAttribute($value):string
    {
        return date('Y-m-d' , $value);
    }

    public function setBirthDateAttribute($value)
    {
        $this->attributes['birth_date'] = strtotime($value);
    }

    public function getDeathDateAttribute($value):string
    {
        return date('Y-m-d' , $value);
    }

    public function setDeathDateAttribute($value)
    {
        $this->attributes['death_date'] = strtotime($value);
    }
}
