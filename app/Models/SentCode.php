<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SentCode extends Model
{
    use HasFactory;
    protected $table='otp_sent_codes';
    protected $fillable = ['user_id' , 'phone' , 'code'];
}
