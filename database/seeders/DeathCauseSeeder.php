<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DeathCauseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('death_causes')->insert([
            [
                'id' => 5,
                'name' => 'بلایای طبیعی'
            ],
            [
                'id' => 1,
                'name' => 'کهولت سن'
            ],
            [
                'id' => 2,
                'name' => 'تصادف'
            ],
            [
                'id' => 3,
                'name' => 'بیماری'
            ],
            [
                'id' => 4,
                'name' => 'حوادث'
            ],

            [
                'id' => 6,
                'name' => 'شهادت'
            ],
            [
                'id' => 999,
                'name' => 'سایر'
            ],
            [
                'id' => 7,
                'name' => 'نامشخص'
            ]
        ]);
    }
}

