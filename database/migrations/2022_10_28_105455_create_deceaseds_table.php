<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDeceasedsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deceaseds', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->constrained()
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('father_name')->nullable();
            $table->bigInteger('birth_date')->comment('timestamp');
            $table->bigInteger('death_date')->comment('timestamp');
            $table->foreignId('deceased_photo')->nullable()->constrained('files' , 'id')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->foreignId('province_id')->constrained()
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->foreignId('city_id')->constrained()
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->string('grave_site');
            $table->string('grave_place')->nullable();
            $table->string('grave_row')->nullable();
            $table->bigInteger('grave_number')->nullable();
            $table->foreignId('death_cause_id')->constrained()
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->text('death_cause_description')->nullable()->comment('if death_cause is of type other');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deceaseds');
    }
}
