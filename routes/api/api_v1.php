<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::middleware('auth:api')->group( function(){
    Route::prefix('profile')->group(function (){
        Route::get('',[\App\Http\Controllers\api\v1\ProfileController::Class , 'show']);
        Route::post('',[\App\Http\Controllers\api\v1\ProfileController::Class , 'store']);
        Route::patch('',[\App\Http\Controllers\api\v1\ProfileController::Class , 'update']);
    });

    Route::prefix('deceased')->middleware('userProfile')->group(function (){
        Route::get('',[\App\Http\Controllers\api\v1\DeceasedController::Class , 'index']);
        Route::get('/{deceased}',[\App\Http\Controllers\api\v1\DeceasedController::Class , 'show']);
        Route::post('',[\App\Http\Controllers\api\v1\DeceasedController::Class , 'store']);
        Route::patch('/{deceased}',[\App\Http\Controllers\api\v1\DeceasedController::Class , 'update']);
        Route::delete('/{deceased}',[\App\Http\Controllers\api\v1\DeceasedController::Class , 'destroy']);
    });

    Route::prefix('upload')->group(function (){
        Route::get('/{upload}',[\App\Http\Controllers\api\v1\UploadController::Class , 'show']);
        Route::post('',[\App\Http\Controllers\api\v1\UploadController::Class , 'store']);
        Route::delete('{upload}',[\App\Http\Controllers\api\v1\UploadController::Class , 'delete']);
    });
});
Route::prefix('province')->group(function(){
    Route::get('/' ,[\App\Http\Controllers\api\v1\ProvinceController::Class , 'index']);
    Route::get('/{province}' ,[\App\Http\Controllers\api\v1\ProvinceController::Class , 'show']);
});
Route::prefix('deceased')->group(function () {
    Route::get('/search', [\App\Http\Controllers\api\v1\DeceasedController::Class, 'search']);
});
Route::prefix('city')->group(function(){
    Route::get('/' ,[\App\Http\Controllers\api\v1\CityController::Class , 'index']);
    Route::get('/{city}' ,[\App\Http\Controllers\api\v1\CityController::Class , 'show']);
});
Route::prefix('otp')->group(function(){
    Route::post('/send-code' ,[\App\Http\Controllers\api\v1\OtpController::Class , 'sendCode']);
    Route::post('/validate-code' ,[\App\Http\Controllers\api\v1\OtpController::Class , 'validateCode']);
});

