<?php


use Illuminate\Support\Facades\Route;

Route::prefix('v1')->namespace('v1')->group(function() {
    require __DIR__.'/api/api_v1.php';
});

